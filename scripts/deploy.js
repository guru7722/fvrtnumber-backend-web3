const { ethers } = require("hardhat");

async function main() {
  //deploying contract
  const LotteryFactory = await ethers.getContractFactory("Lottery");
  console.log("contract deploying . .. . ");
  const Lottery = await LotteryFactory.deploy();
  await Lottery.deployed();
  /*------------------------------------------------------*/

  const output = await retrieveVal(Lottery); //retrieving fvrt number Output from contract
  console.log(output); //outputing it
  await update(2345, Lottery); //updating fvrt number using function declared downbelow
  const updatedoutput = await retrieveVal(Lottery);
  console.log(updatedoutput);
}

//updating asyn function
async function update(val, contract) {
  const update = await contract.store(val);
  await update.wait(1);
}
//retrieving fvrt number output
async function retrieveVal(contract) {
  const output = await contract.retrieve();
  return output.toString();
}

// initializing main function
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
